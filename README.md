# Digits recognition

## Web part

As a wev back-end curently I'm usign PHP (Symfony 3). On the front-end - canvas for drawing. A model can recognize images for 0-9 and only one digit per image.

The project is hosted on [heroku](https://www.heroku.com/). The project's url https://digit-recognition-web.herokuapp.com/. You can also have a look the examples from the previous recognition. 
For instance http://digit-recognition-web.herokuapp.com/img?image-class=2.


## Machine learnign
A model was build using [Keras](https://keras.io/). As training data I used manually written numbers and additionally 
I used augmentation (scrolling, cropping and rotation) for increase dataset. I used simple convolutional neural network. 
The accuracy of the model for the testing dataset is 98%.

There's python script, that does image preparation like reshaping and normalization. You can find script in the folder `scripts/digits_recognition.py`. 

## Installation
For instalations web application you need *PHP7*. Just run `make init_dev`. For prediction script you need *python3* and *pip*.
The command `pip install -r requirements.txt` will install all dependencies.