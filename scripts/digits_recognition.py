#!/usr/bin/python

import os
import sys
import re

from PIL import Image
import numpy as np

from keras.models import load_model

seed = 42
np.random.seed(seed)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

NOT_RECOGNIZED = 'not_recognized'
EMPTY = 'empty'


class DigitsRecognition(object):
    img_rows = 28
    img_cols = 28

    def __init__(self):
        # set root path for the project
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        self.model = load_model('digit_recognition.h5')

    def process_image(self, image_path):
        """
        Processing image for prediction. Saving in temproral folder so that it could be opened by PIL.
        Cropping and scaling so that the longest side it 20. Then putting it in a center of 28x28 blank image.
        Returning array of normalized data.
        """
        img = Image.open(image_path)

        bbox = Image.eval(img, lambda px: 255-px).getbbox()
        if bbox == None:
            return None

        width_len = bbox[2] - bbox[0]
        height_len = bbox[3] - bbox[1]

        if height_len > width_len:
            width_len = int(20.0 * width_len/height_len)
            height_len = 20
        else:
            height_len = int(20.0 * width_len/height_len)
            width_len = 20

        height_start = int((28 - height_len) / 2)
        width_start = int((28 - width_len) / 2)

        img_temp = img.crop(bbox).resize((width_len, height_len), Image.NEAREST)

        new_img = Image.new('L', (28, 28), 255)
        new_img.paste(img_temp, (width_start, height_start))

        return np.array([(255.0 - x) / 255.0 for x in list(new_img.getdata())])

    def recognise(self, img_data, image_path):
        """
        :return: the integer - number, that model recognizes on the image
        """
        image = img_data.reshape(1, self.img_rows, self.img_cols, 1).astype('float32')
        file_name = re.search('.*\/(.*)$', image_path).group(1)

        if self.model.predict_proba(image, verbose=0)[0].max() > 0.90:
            img_class = self.model.predict_classes(image, verbose=0)[0]
            # mode image to the folder
            os.rename(image_path, '../web/images/img_{}/{}'.format(img_class, file_name))

            return img_class
        else:
            # mode image to the folder
            os.rename(image_path, '../web/images/img_not_digit/{}'.format(file_name))

            return NOT_RECOGNIZED


if __name__ == '__main__':
    image_path = sys.argv[1]
    digits_recognition = DigitsRecognition()
    img_data = digits_recognition.process_image(image_path)

    # check if file is empty
    if img_data is None:
        os.remove(image_path)
        response = EMPTY
    else:
        response = digits_recognition.recognise(img_data, image_path)

    print(response)
