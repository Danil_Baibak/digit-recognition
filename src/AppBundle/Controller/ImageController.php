<?php

namespace AppBundle\Controller;

use AppBundle\Services\ImageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends Controller
{
    const STATUS_SUCCESS = 'success';
    const STATUS_EMPTY = 'empty';
    const STATUS_NOT_RECOGNIZED = 'not_recognized';

    /**
     * @Route("/get-image", methods={"POST"})
     */
    public function getImageAction(Request $request)
    {
        $imageRecognition = new ImageService();


        $imagePath = $imageRecognition->base64ToJpeg(
            __DIR__.'/../../../app/tmp',
            $request->request->get('imageBase64')
        );

        $scriptResponse = $this->recognizeDigit($imagePath);

        return new JsonResponse([
            'prediction' => $this->getResponse($scriptResponse)
        ]);
    }

    /**
     * @param $scriptResponse
     * @return string
     */
    private function getResponse($scriptResponse)
    {
        if ($scriptResponse === self::STATUS_EMPTY) {
            return 'The image is empty';
        }

        if ($scriptResponse === self::STATUS_NOT_RECOGNIZED) {
            return 'No digits were found';
        }

        return $scriptResponse;
    }

    /**
     * @param $fileName
     * @return string
     */
    private function recognizeDigit($fileName)
    {
        $commandPath = __DIR__ . '/../../../scripts/digits_recognition.py';
        $command = 'python ' . $commandPath . ' ../app/tmp/' . $fileName;
        $output = shell_exec($command);

        return trim($output);
    }
}
