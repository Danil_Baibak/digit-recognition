<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/about", name="about_page")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/img", name="img_page")
     */
    public function imagesAction(Request $request)
    {
        $imgClass = $request->query->get('image-class');
        $files = scandir(realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR . 'web/images/img_' . $imgClass);

        return $this->render('default/img_list.html.twig', [
            'imgClass' => $imgClass,
            'files' => $files,
        ]);
    }
}
