<?php

namespace AppBundle\Services;

class ImageService
{
    /**
     * Save file from base64 string
     *
     * @param string $path - where to save file
     * @param string $base64String - image in base64
     *
     * @return string - path to saved file
     */
    public function base64ToJpeg($path, $base64String)
    {
        $fileName = md5(uniqid()). '.jpg';
        $outputFile = $path . '/' . $fileName;
        exec('touch ' . $outputFile);

        // open the output file for writing
        $ifp = fopen($outputFile, 'wb');

        // split the string on commas
        $data = explode(',', $base64String);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));
        //clean up the file resource
        fclose($ifp);

        return $fileName;
    }
}
