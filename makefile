init_composer: composer.phar
	export SYMFONY_ENV=${SYMFONY_ENV} && php composer.phar install --prefer-dist -o --no-interaction

composer.phar:
	curl -s https://getcomposer.org/installer | php

init_permissions:
	mkdir var/sessions/dev
	sudo /bin/setfacl -R -m u:www-data:rwX -m u:it:rwX var/cache var/logs var/spool app/tmp
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX var/cache var/logs var/spool app/tmp
	sudo /bin/setfacl -R -m u:www-data:rwX -m u:it:rwX app/var/data
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX app/var/data
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX var/cache/
	sudo /bin/setfacl -R -m u:www-data:rwX -m u:it:rwX var/sessions/dev
	sudo /bin/setfacl -dR -m u:www-data:rwX -m u:it:rwX var/sessions/dev

clean_cache:
	rm -Rf var/cache/*
	rm -Rf app/cache/*
	rm -Rf app/logs/*

clean:
	make clean_cache
	rm -rf app/tmp/*

init_dev: init_permissions clean_cache init_composer
